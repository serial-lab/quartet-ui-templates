"use strict";

/* Copyright (c) 2018 Serial Lab

GNU GENERAL PUBLIC LICENSE
   Version 3, 29 June 2007

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>. */

exports.default = `
.full-form-container > * {
  width:95% !important;
}
`;
/*
.tree-node-output {
  background-image:url("../images/output.svg");
}
.tree-node-output.tree-node-active {
  background-image:url("../images/output-active.svg");
}
.tree-node-endpoints {
  background-image:url("../images/endpoint.svg");
}
.tree-node-endpoints.tree-node-active {
  background-image:url("../images/endpoint-active.svg");
}
.tree-node-epcis-output-criteria {
  background-image:url("../images/epcis-output-criteria.svg");
}
.tree-node-epcis-output-criteria.tree-node-active {
  background-image:url("../images/epcis-output-criteria-active.svg");
}
.tree-node-authentication {
  background-image:url("../images/authentication.svg");
}
.tree-node-authentication.tree-node-active {
  background-image:url("../images/authentication-active.svg");
}
*/